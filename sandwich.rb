require 'prepcook'

class Sandwich

  $available_bread = ["White", "Wheat", "Rye", "Sourdough"]
  $available_meat = ["Roast Beef", "Chicken", "Turkey", "Salami", "Mortadella"]
  $available_veggies = ["Lettuce", "Onions", "Tomatoes", "Pickles", "Cucumber", "Mushrooms"]
  $available_condiments = ["Mustard", "Mayonnaise", "Ketchup", "Cheese"]
  $selected_bread = ""
  $selected_meat = ""
  $selected_veggies = []
  $selected_condiments = []

  def start_order

    puts "Please start with selecting your bread using the 'select_bread('Bread')' command."
    puts "We have #{$available_bread.join(", ")} available."
    
    return self

  end
  
  def select_bread(bread)
    
    favourite_bread = "Sourdough"
	  
    if $available_bread.include?(bread)

      if (bread == favourite_bread)
        puts "Good choice!"
      end

      puts "You have selected #{bread} bread."
      Prepcook.work('slicing', 3, 'bread')
      puts "Please select your meat using the 'select_meat('Meat')' command."
      puts "We have #{$available_meat.join(", ")} available."
      $selected_bread = bread

    else
      puts "Please choose another bread."
      puts "We have #{$available_bread.join(", ")} available. ('select_bread')"
    
    end
    
    return self
  
  end

  def select_meat(meat)

    if $available_meat.include?(meat)
      puts "Your have selected #{meat} for your sandwich."
      Prepcook.work('shaving', 4, meat)
      puts "Please select your veggies using the 'select_veggie('Veggie')' command."
      puts "We have #{$available_veggies.join(", ")} available."
      $selected_meat = meat

    else
      puts "We do not have that in stock at this time."
      puts "Please select from: #{$available_meat.join(", ")} ('select_meat')."

    end

    return self
							        
  end

  def select_veggie(veggie)

    max_veggie = 3

    if $selected_veggies.count < max_veggie

      if $available_veggies.include?(veggie)
        puts "You have selected #{veggie}."
        Prepcook.work('stacking', 1, veggie)
        puts "Please select another veggie using 'select_veggie', or select your condiments using the 'select_condiment('Condiment')' command."
        puts "Veggies: #{$available_veggies.join(", ")}."
        puts "Condiments: #{$available_condiments.join(", ")}."
        $selected_veggies << veggie

      else
        Prepcook.work('checking', 2, "for #{veggie}")  
        puts "We do not have #{veggie} available at this time."
        puts "Please select from #{$available_veggies.join(", ")} ('select_veggie')."
	
      end

    else
      puts "You have reached your veggie maximum! (#{max_veggie.to_s})."
      puts "Please select a condiment ('select_condiment('Condiment')')."

    end

    return self

  end

  def select_condiment(condiment)
    
    if $available_condiments.include?(condiment)
      puts "You have selected #{condiment}."
      Prepcook.work('spreading', 1, condiment)
      puts "Please select another condiment ('select_condiment') or order your sandwich ('place_order')"
      puts "Condiments: #{$available_condiments.join(", ")}."
      $selected_condiments << condiment

    else
      puts "We do not have that available at this time."
      puts "Please select from #{$available_condiments.join(", ")} ('select_condiment')."
    
    end
  
    return self

  end
  
  def place_order
  
    veggie_redux = ""
    condiment_redux = ""

    if $selected_bread == ""
      puts "You have not selected any bread."
      puts "Please choose from #{$available_bread.join(", ")}."

    elsif $selected_meat == ""
      puts "You have not selected any meat."
      puts "Please choose from #{$available_meat.join(", ")}."
    
    else
      veggie_redux = $selected_veggies.join(", ") if $selected_veggies.count > 0
      condiment_redux = $selected_condiments.join(", ") if $selected_condiments.count > 0

      Prepcook.work('assembling', 6, "your sandwich")
      puts "You have ordered a #{$selected_meat} sandwich on #{$selected_bread} bread"
      puts "with #{veggie_redux} for vegetables" if veggie_redux != ""
      puts "and #{condiment_redux} for condiments" if condiment_redux != ""

    end

    return self
  
  end

end
